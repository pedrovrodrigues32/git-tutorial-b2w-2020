import json
import requests

from flask import Flask, request
app = Flask(__name__)


def get_product_info(pid):
    url = 'http://product-v3-americanas-npf.internal.b2w.io/product/{pid}'
    response = requests.get(url.format(pid=pid))
    if response.ok:
        return response.json()
    else:
        raise RuntimeError('Erro na obtenção do produto: ' + pid)


def get_offer_info(oid):
    url = 'http://offer-v1-americanas-npf.internal.b2w.io/offer/{oid}'
    response = requests.get(url.format(oid=oid))
    if response.ok:
        return response.json()
    else:
        raise RuntimeError('Erro na obtenção da offer: ' + oid)


def get_info_id_all_products(products):
    p_infos_id = []
    for product in products:
        pid = product['id']
        product_data = get_product_info(pid)
        p_info = {
            'name': product_data['name'],
            'rating': product_data['rating']
        }
        p_infos_id.append((p_info, pid))
    return p_infos_id


def calculate_products_metrics(products):
    metrics = {}
    p_infos_id = get_info_id_all_products(products)
    for p_info, pid in p_infos_id:
        best_rated = metrics.get('best_rated', {})
        best_rating = best_rated.get('rating', 0)
        if best_rating < p_info['rating']['average']:
            best_rated['pid'] = pid
            best_rated['rating'] = p_info['rating']['average']
            metrics['best_rated'] = best_rated

        metrics[pid] = p_info

    return metrics


@app.route('/product/<pid>')
def product(pid):
    return get_product_info(pid)


@app.route('/products/metrics', methods=['GET', 'POST'])
def products_metrics():
    data = request.get_json()
    return calculate_products_metrics(data['products'])


@app.route('/offer/<oid>')
def offer(oid):
    return get_offer_info(oid)


if __name__ == '__main__':
    app.run(debug=True)
